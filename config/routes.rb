Shoppe::Engine.routes.draw do
  get 'attachment/:id/:filename.:extension' => 'attachments#show'

  resources :customers do
    post :search, :on => :collection
    resources :addresses
  end

  resources :blogposts
  resources :currencies
  resources :purchased_items

  resources :product_categories do
    resources :localisations, controller: "product_category_localisations"
  end
  resources :products do
    resources :variants
    resources :localisations, controller: "product_localisations"
    collection do
      get :import
      post :import
    end
  end
  resources :orders do
    collection do
      post :search
      post :submit_to_admin
      post :mass_process
    end
    member do
      delete :remove_order_purchased_item
      post :add_order_purchased_item
      post :assign_to_me
      post :accept
      post :reject
      post :ship
      get :despatch_note
      post :archive
      post :calculate_commission
    end
    resources :payments, :only => [:create, :destroy] do
      match :refund, :on => :member, :via => [:get, :post]
    end
  end
  resources :stock_level_adjustments, :only => [:index, :create]
  resources :delivery_services do
    resources :delivery_service_prices
  end
  resources :tax_rates
  resources :users do
    collection do
      get :sales_reps
      get :my_account
    end
  end
  resources :countries
  resources :attachments, :only => :destroy

  get 'settings'=> 'settings#edit'
  post 'settings' => 'settings#update'

  get 'permissions'=> 'permissions#edit', as: "edit_permissions"
  patch 'permissions' => 'permissions#update', as: "update_permissions"

  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  get 'authenticate' => 'sessions#second_authentication', as: "second_authentication"
  post 'perform_second_authentication' => 'sessions#perform_second_authentication', as: "perform_second_authentication"

  match 'login/reset' => 'sessions#reset', :via => [:get, :post]

  delete 'logout' => 'sessions#destroy'
  root :to => 'dashboard#home'
end
