module Shoppe
  class OrdersController < Shoppe::ApplicationController

    before_filter { @active_nav = :orders unless params[:submitted]}
    before_filter { params[:id] && @order = Shoppe::Order.find(params[:id])}
    before_filter :verify_access

    def index
      @query = Shoppe::Order.ordered.received.includes(:order_items => :ordered_item).page(params[:page]).search(params[:q])
      @orders = @query.result
      @assigned_users = Shoppe::User.where(id: (@orders.collect(&:user_id).uniq - [nil, current_user.id]))
    end

    def new
      @order = Shoppe::Order.new
      @order.order_items.build(:ordered_item_type => 'Shoppe::Product')
    end

    def assign_to_me
      @order = Shoppe::Order.find params[:id]
      return if @order.assigned?
      @order.update_attributes(user: current_user)
      redirect_to :back, notice: "Order has been successfully assigned to you"
    end

    def create
      Shoppe::Order.transaction do
        @order = Shoppe::Order.new(safe_params)
        @order.status = 'confirming'

        if safe_params[:customer_id].present?
          @customer = Shoppe::Customer.find safe_params[:customer_id]
          @order.first_name = @customer.first_name
          @order.last_name = @customer.last_name
          @order.company = @customer.company
          @order.email_address = @customer.email
          @order.phone_number = @customer.phone
          if @customer.addresses.billing.present?
            billing = @customer.addresses.billing.first
            @order.billing_address1 = billing.address1
            @order.billing_address2 = billing.address2
            @order.billing_address3 = billing.address3
            @order.billing_address4 = billing.address4
            @order.billing_postcode = billing.postcode
            @order.billing_country_id = billing.country_id
          end
          if @customer.addresses.delivery.present?
            delivery = @customer.addresses.delivery.first
            @order.delivery_address1 = delivery.address1
            @order.delivery_address2 = delivery.address2
            @order.delivery_address3 = delivery.address3
            @order.delivery_address4 = delivery.address4
            @order.delivery_postcode = delivery.postcode
            @order.delivery_country_id = delivery.country_id
          end
        end

        if !request.xhr? && @order.save
          @order.confirm!
          redirect_to @order, :notice => t('shoppe.orders.create_notice')
        else
          @order.order_items.build(:ordered_item_type => 'Shoppe::Product')
          render :action => "new"
        end
      end
    rescue Shoppe::Errors::InsufficientStockToFulfil => e
      flash.now[:alert] = t('shoppe.orders.insufficient_stock_order', out_of_stock_items: e.out_of_stock_items.map { |t| t.ordered_item.full_name }.to_sentence)
      render :action => 'new'
    end

    def show
      @payments = @order.payments.to_a
      @order_purchased_items = @order.order_purchased_items
      @possible_purchased_items = Shoppe::PurchasedItem.for_products(@order.order_items.collect{|i| i.ordered_item_id})
      @commissions = @order.commissions
    end

    def update
      @order.attributes = safe_params
      if !request.xhr? && @order.update_attributes(safe_params)
        redirect_to @order, :notice => t('shoppe.orders.update_notice')
      else
        render :action => "edit"
      end
    end

    def search
      index
      render :action => "index"
    end

    def accept
      @order.accept!(current_user)
      redirect_to @order, :notice => t('shoppe.orders.accept_notice')
    rescue Shoppe::Errors::PaymentDeclined => e
      redirect_to @order, :alert => e.message
    end

    def reject
      @order.reject!(current_user)
      redirect_to @order, :notice => t('shoppe.orders.reject_notice')
    rescue Shoppe::Errors::PaymentDeclined => e
      redirect_to @order, :alert => e.message
    end

    def ship
      @order.ship!(params[:consignment_number], current_user)
      redirect_to @order, :notice => t('shoppe.orders.ship_notice')
    end

    def despatch_note
      render :layout => 'shoppe/printable'
    end

    def add_order_purchased_item
      @order = Shoppe::Order.find params[:id]
      redirect_to root_path and return unless @order
      @opi = @order.order_purchased_items.new(order_purchased_item_params)
      purchased_item = Shoppe::PurchasedItem.where(id: @opi.purchased_item_id).first
      redirect_to @order and return unless purchased_item
      redirect_to @order, alert: "Number of items used cannot exceed the number available" and return if @opi.purchased_item_qty > purchased_item.available_quantity
      redirect_to @order, alert: "Number of items used cannot exceed the number of items ordered" and return if @opi.purchased_item_qty > (@order.order_items.where(ordered_item_id: purchased_item.shoppe_product_id).first.try(:quantity).try(:to_i) - @order.order_purchased_items.for_product(purchased_item.shoppe_product_id).sum(:purchased_item_qty))

      purchased_item.update_attributes(quantity_sold: purchased_item.quantity_sold+@opi.purchased_item_qty) if @opi.save

      redirect_to @order, notice: "Added successfully"
    end

    def remove_order_purchased_item
      @order = Shoppe::Order.find params[:id]
      redirect_to root_path and return unless @order
      @opi = @order.order_purchased_items.where(id: params[:order_purchased_item_id]).first
      redirect_to root_path and return unless @opi
      purchased_item = Shoppe::PurchasedItem.where(id: @opi.purchased_item_id).first
      redirect_to @order and return unless purchased_item

      purchased_item.update_attributes(quantity_sold: purchased_item.quantity_sold-@opi.purchased_item_qty) if @opi.destroy

      redirect_to @order, notice: "Removed successfully"
    end

    def submit_to_admin
      Shoppe::Order.where(id: params[:order_ids]).where(admin_status: nil).update_all(admin_status: "submitted")
      redirect_to orders_path(active_tab: "my_orders"), notice: "Submitted Successfully"
    end

    def archive
      redirect_to root_path unless current_user.admin?
      @order = Shoppe::Order.find params[:id]
      redirect_to root_path and return unless @order
      @order.update_attributes(admin_status: "archived") if @order.admin_status == "submitted"
      redirect_to @order, notice: "Archived Successfully"
    end

    def calculate_commission
      redirect_to root_path unless current_user.admin?
      @order = Shoppe::Order.find params[:id]
      redirect_to root_path and return unless @order
      redirect_to @order, alert: "Order must be assigned to a user before calculating the commission" and return unless @order.user.present?
      @order.recalculate_commission
      redirect_to @order, notice: "Calculated Successfully"
    end

    def mass_process
      Shoppe::Order.where(id: params[:order_ids]).where(admin_status: "submitted").each do |o|
        if params[:process_type] == "commission"
          o.recalculate_commission if o.user.present?
        elsif params[:process_type] == "archive"
          o.update_attributes(admin_status: "archived") if o.admin_status == "submitted"
        end
      end

      redirect_to orders_path(submitted: true), notice: "Processed Successfully"
    end

    private

    def safe_params
      params[:order].permit(
        :customer_id,
        :first_name, :last_name, :company,
        :billing_address1, :billing_address2, :billing_address3, :billing_address4, :billing_postcode, :billing_country_id,
        :separate_delivery_address,
        :delivery_name, :delivery_address1, :delivery_address2, :delivery_address3, :delivery_address4, :delivery_postcode, :delivery_country_id,
        :delivery_price, :delivery_service_id, :delivery_tax_amount,
        :email_address, :phone_number,
        :notes, :user_id, :payment_type_id,
        :order_items_attributes => [:ordered_item_id, :ordered_item_type, :quantity, :unit_price, :tax_amount, :id, :weight]
      )
    end

    def order_purchased_item_params
      params[:order_purchased_item].permit(:purchased_item_id, :purchased_item_qty)
    end

    def verify_access
      redirect_to root_path, alert: "Operation not permitted" unless can_manage("orders")
    end
  end
end
