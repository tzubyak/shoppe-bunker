module Shoppe
  class SettingsController < ApplicationController

    before_filter { @active_nav = :settings }
    before_filter :verify_access

    def update
      if Shoppe.settings.demo_mode?
        raise Shoppe::Error, t('shoppe.settings.demo_mode_error')
      end

      Shoppe::Setting.update_from_hash(params[:settings].permit!)
      redirect_to :settings, :notice => t('shoppe.settings.update_notice')
    end

    def verify_access
      redirect_to root_path, alert: "Operation not permitted" unless current_user.admin?
    end

  end
end
