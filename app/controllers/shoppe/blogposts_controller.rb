module Shoppe
  class BlogpostsController < Shoppe::ApplicationController

    before_filter { @active_nav = :blogposts }
    before_filter { params[:id] && @blogpost = Shoppe::Blogpost.find(params[:id])}

    before_filter :verify_access

    def index
      @blogposts = Shoppe::Blogpost.all
    end

    def new
      @blogpost = Shoppe::Blogpost.new
    end

    def show
    end

    def create
      @blogpost = Shoppe::Blogpost.new(safe_params)
      @blogpost.published_at = DateTime.now if @blogpost.status == "Published"
      if @blogpost.save
        redirect_to edit_blogpost_path(@blogpost), :flash => {:notice => "Blogpost has been created successfully"}
      else
        render :action => "new"
      end
    end

    def update
      old_status = @blogpost.status
      @blogpost.assign_attributes(safe_params)
      @blogpost.published_at = DateTime.now if @blogpost.status != old_status && @blogpost.status == "Published" && !@blogpost.published_at.present?
      if @blogpost.update(safe_params)

        redirect_to edit_blogpost_path(@blogpost), :flash => {:notice => "Blogpost has been updated successfully"}
      else
        render :action => "edit"
      end
    end

    def destroy
      @blogpost.destroy
      redirect_to blogposts_path, :flash => {:notice => "Blogpost has been deleted successfully"}
    end

    private

    def safe_params
      params[:blogpost].permit(:title, :short_description, :body, :status, :author_id, :published_at, :featured_image)
    end

    def verify_access
      redirect_to root_path, alert: "Operation not permitted" unless can_manage("blogposts")
    end

  end
end
