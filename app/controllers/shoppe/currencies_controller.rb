module Shoppe
  class CurrenciesController < Shoppe::ApplicationController

    before_filter { @active_nav = :currencies }
    before_filter { params[:id] && @currency = Shoppe::Currency.find(params[:id])}

    before_filter :verify_access

    def index
      @currencies = Shoppe::Currency.all
    end

    def edit
    end

    def update
      if @currency.update_attributes(safe_params)
        redirect_to currencies_path, :flash => {:notice => "Currency has been updated successfully"}
      else
        render :action => "edit"
      end
    end

    private

    def safe_params
      params[:currency].permit(:base_multiplier)
    end

    def verify_access
      redirect_to root_path, alert: "Operation not permitted" unless can_manage("currencies")
    end

  end
end
