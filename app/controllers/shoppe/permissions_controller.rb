module Shoppe
  class PermissionsController < ApplicationController

    before_filter :verify_access

    def edit
      @permission = Shoppe::Permission.where(role_name: "Sales Rep").first
    end

    def update
      @permission = Shoppe::Permission.where(role_name: "Sales Rep").first
      @permission.update_attributes permission_params
      redirect_to edit_permissions_path, notice: "Saved successfully"
    end

    private

    def permission_params
      params.require(:permission).permit!
    end

    def verify_access
      redirect_to root_path, alert: "Operation not permitted" unless current_user.admin?
    end

  end
end
