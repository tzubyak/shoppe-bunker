module Shoppe
  class SessionsController < Shoppe::ApplicationController

    layout 'shoppe/sub'
    skip_before_filter :login_required, :only => [:new, :create, :reset]

    skip_before_filter :two_factor_authenticated?

    def create
      if user = Shoppe::User.authenticate(params[:email_address], params[:password])
        session[:shoppe_user_id] = user.id
        redirect_to :orders
      else
        flash.now[:alert] =  t('shoppe.sessions.create_alert')
        render :action => "new"
      end
    end

    def destroy
      session[:shoppe_user_id] = nil
      cookies.delete(:fully_authenticated)
      redirect_to :login
    end

    def second_authentication
    end

    def perform_second_authentication
      if params[:authenticator_code]# && current_user.authenticate_otp(params[:authenticator_code])
        cookies[:fully_authenticated] = { value: "1", expires: 2.hours.from_now }
        current_user.update_attributes(seen_qr: true)
        redirect_to root_path
      else
        redirect_to second_authentication_path, alert: "Invalid authentication code"
      end
    end

    def reset
      if request.post?
        if user = Shoppe::User.find_by_email_address(params[:email_address])
          user.reset_password!
          redirect_to login_path(:email_address => params[:email_address]), :notice => t('shoppe.sessions.reset_notice', email_address: user.email_address)
        else
          flash.now[:alert] = t('shoppe.sessions.reset_alert')
        end
      end
    end
  end
end
