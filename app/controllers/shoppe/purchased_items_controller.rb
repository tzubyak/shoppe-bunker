module Shoppe
  class PurchasedItemsController < Shoppe::ApplicationController

    before_filter { @active_nav = :purchased_items }
    before_filter { params[:id] && @item = Shoppe::PurchasedItem.find(params[:id])}

    before_filter :verify_access

    def index
      @items = Shoppe::PurchasedItem.active
      @my_items = current_user.purchased_items
    end

    def new
      @item = Shoppe::PurchasedItem.new
    end

    def show
    end

    def create
      @item = Shoppe::PurchasedItem.new(safe_params)
      if @item.save
        @item.product.stock_level_adjustments.create(description: "Purchased Items from the client", adjustment: @item.quantity_bought)
        redirect_to @item, :flash => {:notice => "Purchased Item has been created successfully"}
      else
        render :action => "new"
      end
    end

    def update
      if @item.update(safe_params)
        redirect_to @item, :flash => {:notice => "Purchased Item has been updated successfully"}
      else
        render :action => "edit"
      end
    end

    def destroy
      @item.destroy
      redirect_to purchased_items_path, :flash => {:notice => "Purchased Item has been deleted successfully"}
    end

    private

    def safe_params
      params[:purchased_item].permit(:shoppe_product_id, :shoppe_user_id, :shoppe_customer_id, :price, :quantity_bought, :payment_type_id, :notes)
    end

    def verify_access
      redirect_to root_path, alert: "Operation not permitted" unless can_manage("purchased_items")
    end

  end
end
