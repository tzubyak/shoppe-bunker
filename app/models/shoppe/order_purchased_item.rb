module Shoppe
  class OrderPurchasedItem < ActiveRecord::Base

    self.table_name = "shoppe_order_purchased_items"

    belongs_to :order, class_name: "Shoppe::Order", foreign_key: "order_id"
    belongs_to :purchased_item, class_name: "Shoppe::PurchasedItem", foreign_key: "purchased_item_id"

    validates :order_id, :purchased_item_id, :purchased_item_qty, presence: true

    scope :for_product, ->(ids) { joins(:purchased_item).where(shoppe_purchased_items: {shoppe_product_id: ids}) }
  end
end
