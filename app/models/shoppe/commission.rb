module Shoppe
  class Commission < ActiveRecord::Base

    self.table_name = "shoppe_commissions"

    belongs_to :user, class_name: "Shoppe::User", foreign_key: "user_id"
    belongs_to :order, class_name: "Shoppe::Order", foreign_key: "order_id"
    belongs_to :order_purchased_item, class_name: "Shoppe::OrderPurchasedItem", foreign_key: "order_purchased_item_id"

    validates :commission_type, :commission_amount, :user_id, :order_id, presence: true

    scope :published, -> { where(status: "published").order(published_at: :desc)}
  end
end
