module Shoppe
  class PurchasedItem < ActiveRecord::Base

    self.table_name = "shoppe_purchased_items"

    belongs_to :product, class_name: "Shoppe::Product", foreign_key: "shoppe_product_id"
    belongs_to :user, class_name: "Shoppe::User", foreign_key: "shoppe_user_id"
    belongs_to :customer, class_name: "Shoppe::Customer", foreign_key: "shoppe_customer_id"
    belongs_to :payment_type, class_name: "Shoppe::PaymentType", foreign_key: "payment_type_id"

    validates :shoppe_product_id, :shoppe_user_id, :shoppe_customer_id, :price, :quantity_bought, :payment_type_id, presence: true

    scope :active, -> { where("quantity_bought > 0 AND quantity_bought > quantity_sold").order(created_at: :desc)}
    scope :for_products, ->(ids) { where(shoppe_product_id: ids).order(created_at: :asc).active }

    def available?
      quantity_bought.to_i > quantity_sold.to_i
    end

    def available_quantity
      quantity_bought.to_i - quantity_sold.to_i
    end

    def description
      "#{product.full_name} --- | Purchased: #{created_at.to_s(:short)} | Qty Available: #{available_quantity} |"
    end
  end
end
