module Shoppe
  class PaymentType < ActiveRecord::Base
    has_many :orders, :class_name => 'Shoppe::Order'
    has_many :purchased_items, :class_name => 'Shoppe::PurchasedItems'
  end
end
