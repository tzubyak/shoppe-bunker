module Shoppe
  class User < ActiveRecord::Base

    self.table_name = 'shoppe_users'

    has_secure_password
    has_one_time_password

    has_many :purchased_items, foreign_key: "shoppe_user_id"
    has_many :commissions, class_name: "Shoppe::Commission", foreign_key: "user_id", :dependent => :destroy

    # Validations
    validates :first_name, :presence => true
    validates :last_name, :presence => true
    validates :email_address, :presence => true

    scope :sales_reps, -> { where(user_type: "Sales Rep") }

    # The user's first name & last name concatenated
    #
    # @return [String]
    def full_name
      "#{first_name} #{last_name}"
    end

    # The user's first name & initial of last name concatenated
    #
    # @return [String]
    def short_name
      "#{first_name} #{last_name[0,1]}"
    end

    def active_commissions
      commissions.select{|c| c.order.admin_status != "archived"}
    end

    def archived_commissions
      commissions.select{|c| c.order.admin_status == "archived"}
    end

    # Reset the user's password to something random and e-mail it to them
    def reset_password!
      self.password = SecureRandom.hex(8)
      self.password_confirmation = self.password
      self.save!
      Shoppe::UserMailer.new_password(self).deliver_now
    end

    # Attempt to authenticate a user based on email & password. Returns the
    # user if successful otherwise returns false.
    #
    # @param email_address [String]
    # @param paassword [String]
    # @return [Shoppe::User]
    def self.authenticate(email_address, password)
      user = self.where(:email_address => email_address).first
      return false if user.nil?
      return false unless user.authenticate(password)
      user
    end

    def admin?
      user_type.blank? || user_type == "Admin"
    end

  end
end
