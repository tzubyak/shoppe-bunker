module Shoppe
  class Currency < ActiveRecord::Base

    self.table_name = "shoppe_currencies"

    validates :code, :base_multiplier, presence: true

    def self.sync_currencies
      fx = OpenExchangeRates::Rates.new
      self.all.each do |c|
        c.update_attributes(base_multiplier: fx.exchange_rate(from: "CAD", to: c.code))
      end
    end

  end
end
