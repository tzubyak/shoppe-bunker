module Shoppe
  class Order < ActiveRecord::Base

    self.table_name = 'shoppe_orders'

    # Orders can have properties
    key_value_store :properties

    # Require dependencies
    require_dependency 'shoppe/order/states'
    require_dependency 'shoppe/order/actions'
    require_dependency 'shoppe/order/billing'
    require_dependency 'shoppe/order/delivery'

    # All items which make up this order
    has_many :order_items, :dependent => :destroy, :class_name => 'Shoppe::OrderItem', :inverse_of => :order
    accepts_nested_attributes_for :order_items, :allow_destroy => true, :reject_if => Proc.new { |a| a['ordered_item_id'].blank? }

    # All products which are part of this order (accessed through the items)
    has_many :products, :through => :order_items, :class_name => 'Shoppe::Product', :source => :ordered_item, :source_type => 'Shoppe::Product'

    # The order can belong to a customer
    belongs_to :customer, :class_name => 'Shoppe::Customer'
    belongs_to :user, :class_name => 'Shoppe::User'
    belongs_to :payment_type, :class_name => 'Shoppe::PaymentType'
    has_many :addresses, :through => :customers, :class_name => "Shoppe::Address"

    has_many :order_purchased_items, class_name: "Shoppe::OrderPurchasedItem", foreign_key: "order_id", :dependent => :destroy
    has_many :commissions, class_name: "Shoppe::Commission", foreign_key: "order_id", :dependent => :destroy

    # Validations
    validates :token, :presence => true
    with_options :if => Proc.new { |o| !o.building? } do |order|
      order.validates :email_address, :format => {:with => /\A\b[A-Z0-9\.\_\%\-\+]+@(?:[A-Z0-9\-]+\.)+[A-Z]{2,6}\b\z/i}
    end

    # Set some defaults
    before_validation { self.token = SecureRandom.uuid  if self.token.blank? }

    scope :assigned_to, ->(user) { where(user_id: user.id) }
    scope :unassigned, -> { where(user_id: nil) }
    scope :archived, -> { where(admin_status: "archived") }
    scope :submitted, -> { where(admin_status: "submitted") }
    scope :active, -> { where("admin_status IS NULL OR admin_status <> ?", "archived") }

    # Some methods for setting the billing & delivery addresses
    attr_accessor :save_addresses, :billing_address_id, :delivery_address_id

    # The order number
    #
    # @return [String] - the order number padded with at least 5 zeros
    def number
      id ? id.to_s.rjust(6, '0') : nil
    end

    # The length of time the customer spent building the order before submitting it to us.
    # The time from first item in basket to received.
    #
    # @return [Float] - the length of time
    def build_time
      return nil if self.received_at.blank?
      self.created_at - self.received_at
    end

    # The name of the customer in the format of "Company (First Last)" or if they don't have
    # company specified, just "First Last".
    #
    # @return [String]
    def customer_name
      company.blank? ? full_name : "#{company} (#{full_name})"
    end

    # The full name of the customer created by concatinting the first & last name
    #
    # @return [String]
    def full_name
      "#{first_name} #{last_name}"
    end

    # Is this order empty? (i.e. doesn't have any items associated with it)
    #
    # @return [Boolean]
    def empty?
      order_items.empty?
    end

    # Does this order have items?
    #
    # @return [Boolean]
    def has_items?
      total_items > 0
    end

    # Return the number of items in the order?
    #
    # @return [Integer]
    def total_items
      order_items.inject(0) { |t,i| t + i.quantity }
    end

    def assigned?
      user.present?
    end

    def self.ransackable_attributes(auth_object = nil)
      ["id", "billing_postcode", "billing_address1", "billing_address2", "billing_address3", "billing_address4", "first_name", "last_name", "company", "email_address", "phone_number", "consignment_number", "status", "received_at"] + _ransackers.keys
    end

    def self.ransackable_associations(auth_object = nil)
      []
    end

    def recalculate_commission
      commissions.destroy_all

      items_to_purchased = {}.tap do |hsh|
        order_items.each do |item|
          hsh[item.id] = order_purchased_items.select{|opi| opi.purchased_item.shoppe_product_id == item.ordered_item_id}
        end
      end

      order_items.each do |item|
        if items_to_purchased[item.id].present? #both buyer and seller should get a commission
          qty_to_sell = item.quantity
          items_to_purchased[item.id].each do |purchased|
            profit = purchased.purchased_item_qty * calc_profit(item.unit_price, purchased.purchased_item.price, payment_type.try(:slug))
            process_single_commission(user, "seller", profit)
            process_single_commission(purchased.purchased_item.user, "buyer", profit)
            qty_to_sell -= purchased.purchased_item_qty
          end
          if qty_to_sell > 0
            profit = qty_to_sell * calc_profit(item.unit_price, item.unit_cost_price, payment_type.try(:slug))
            process_single_commission(user, "seller", profit)
          end
        else #only seller gets commission
          profit = item.quantity * calc_profit(item.unit_price, item.unit_cost_price, payment_type.try(:slug))
          process_single_commission(user, "seller", profit)
        end
      end
    end

    def admin_status_str
      case admin_status
      when "submitted"
        "Submitted to Admin"
      when "archived"
        "Archived by Admin"
      else
        "In Process"
      end
    end

    private

    def process_single_commission(comm_user, comm_type, profit)
      comm = commissions.where(user_id: comm_user.id, commission_type: comm_type).first_or_initialize
      comm.commission_amount = comm.commission_amount.to_f + (profit * (comm_user.send("#{comm_type}_cut") || 0) / 100.0)
      comm.save
    end

    def calc_profit(price, cost, pay_type)
      case pay_type
      when "paypal"
        return calc_paypal_profit(price, cost)
      when "skrill"
        return calc_skrill_profit(price, cost)
      else
        return calc_other_profit(price, cost)
      end
    end

    def calc_paypal_profit price, cost
      (price.to_f * 0.981) - 0.3 - (cost * 1.01)
    end

    def calc_skrill_profit price, cost
      (price.to_f * 0.951) - 0.3 - (cost * 1.01)
    end

    def calc_other_profit price, cost
      price.to_f - (cost * 1.01)
    end
  end
end
